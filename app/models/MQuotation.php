<?php

class MQuotation extends ActiveRecord
{

	public function tableName()
	{
		return 'm_quotation';
	}

	public function rules()
	{
		return array(
			array('name, m_customer_id', 'required'),
			array('m_customer_id, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('additional_discounted_price', 'numerical'),
			array('name', 'length', 'max'=>100),
			array('created_date, updated_date', 'safe'),
		);
	}

	public function relations()
	{
		return array(
			'mCustomer' => array(self::BELONGS_TO, 'MCustomer', 'm_customer_id'),
			'mQuotationDetails' => array(self::HAS_MANY, 'MQuotationDetails', 'm_quotation_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'm_customer_id' => 'M Customer',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
			'additional_discounted_price' => 'Additional Discounted Price',
		);
	}

}
