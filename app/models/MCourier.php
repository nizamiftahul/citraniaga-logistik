<?php

class MCourier extends ActiveRecord
{

	public function tableName()
	{
		return 'm_courier';
	}

	public function rules()
	{
		return array(
			array('name, m_courier_type_id, p_user_id', 'required'),
			array('m_courier_type_id, created_by, updated_by, p_user_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
			array('created_date, updated_date', 'safe'),
		);
	}

	public function relations()
	{
		return array(
			'mCourierType' => array(self::BELONGS_TO, 'MCourierType', 'm_courier_type_id'),
			'pUser' => array(self::BELONGS_TO, 'User', 'p_user_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Nama',
			'm_courier_type_id' => 'Tipe Kontainer',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
			'p_user_id' => 'User',
		);
	}

}
