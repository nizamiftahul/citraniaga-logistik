<?php

class MCost extends ActiveRecord
{

	public function tableName()
	{
		return 'm_cost';
	}

	public function rules()
	{
		return array(
			array('name, m_vendor_id, m_container_type_id, pol_cost, pod_cost, other_cost, tax_cost, trucking_pol, pelindo_pod, worker_pod, dooring_pod, trucking_pod, insurance, ppn_enabled, m_cost_type_id', 'required'),
			array('m_vendor_id, m_container_type_id, pol_cost, pod_cost, other_cost, tax_cost, trucking_pol, pelindo_pod, worker_pod, dooring_pod, trucking_pod, insurance, m_cost_type_id, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>45),
			array('description, created_date, updated_date', 'safe'),
		);
	}

	public function relations()
	{
		return array(
			'mQuotationDetails' => array(self::HAS_MANY, 'MQuotationDetails', 'm_cost_id'),
			'mVendor' => array(self::BELONGS_TO, 'MVendor', 'm_vendor_id'),
			'mContainerType' => array(self::BELONGS_TO, 'MContainerType', 'm_container_type_id'),
			'mCostType' => array(self::BELONGS_TO, 'MCostType', 'm_cost_type_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Nama',
			'description' => 'Deskirpsi',
			'm_vendor_id' => 'Vendor',
			'm_container_type_id' => 'Tipe Kontainer',
			'pol_cost' => 'Pol Cost',
			'pod_cost' => 'Pod Cost',
			'other_cost' => 'Other Cost',
			'tax_cost' => 'Tax Cost',
			'trucking_pol' => 'Trucking Pol',
			'pelindo_pod' => 'Pelindo Pod',
			'worker_pod' => 'Worker Pod',
			'dooring_pod' => 'Dooring Pod',
			'trucking_pod' => 'Trucking Pod',
			'insurance' => 'Asuransi',
			'ppn_enabled' => 'PPN',
			'm_cost_type_id' => 'Tipe Biaya',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

}
