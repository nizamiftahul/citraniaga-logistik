<?php

class MRoutePath extends ActiveRecord
{

	public function tableName()
	{
		return 'm_route_path';
	}

	public function rules()
	{
		return array(
			array('m_route_id, m_orde_id', 'required'),
			array('m_route_id, m_orde_id', 'numerical', 'integerOnly'=>true),
		);
	}

	public function relations()
	{
		return array(
			'mRoute' => array(self::BELONGS_TO, 'MRoute', 'm_route_id'),
			'mOrde' => array(self::BELONGS_TO, 'MOrde', 'm_orde_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'm_route_id' => 'M Route',
			'm_orde_id' => 'M Orde',
		);
	}

}
