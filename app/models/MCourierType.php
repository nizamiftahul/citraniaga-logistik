<?php

class MCourierType extends ActiveRecord
{

	public function tableName()
	{
		return 'm_courier_type';
	}

	public function rules()
	{
		return array(
			array('name', 'required'),
			array('created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
			array('created_date, updated_date', 'safe'),
		);
	}

	public function relations()
	{
		return array(
			'mCouriers' => array(self::HAS_MANY, 'MCourier', 'm_courier_type_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Tipe',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

}
