<?php

class MVessel extends ActiveRecord
{

	public function tableName()
	{
		return 'm_vessel';
	}

	public function rules()
	{
		return array(
			array('name', 'required'),
			array('created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('name, imo', 'length', 'max'=>45),
			array('description, created_date, updated_date', 'safe'),
		);
	}

	public function relations()
	{
		return array(
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Nama',
			'description' => 'Deskirpsi',
			'imo' => 'IMO',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

}
