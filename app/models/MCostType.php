<?php

class MCostType extends ActiveRecord
{

	public function tableName()
	{
		return 'm_cost_type';
	}

	public function rules()
	{
		return array(
			array('name, description', 'required'),
			array('created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
			array('created_date, updated_date', 'safe'),
		);
	}

	public function relations()
	{
		return array(
			'mCosts' => array(self::HAS_MANY, 'MCost', 'm_cost_type_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Tipe',
			'description' => 'Deskirpsi',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

}
