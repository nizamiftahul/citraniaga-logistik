<?php

class MProduct extends ActiveRecord
{

	public function tableName()
	{
		return 'm_product';
	}

	public function rules()
	{
		return array(
			array('name, m_product_type_id, m_customer_id, unit_price', 'required'),
			array('created_by, updated_by, m_product_type_id, m_customer_id', 'numerical', 'integerOnly'=>true),
			array('unit_price', 'numerical'),
			array('name', 'length', 'max'=>100),
			array('description, created_date, updated_date', 'safe'),
		);
	}

	public function relations()
	{
		return array(
			'mProductType' => array(self::BELONGS_TO, 'MProductType', 'm_product_type_id'),
			'mCustomer' => array(self::BELONGS_TO, 'MCustomer', 'm_customer_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Nama',
			'description' => 'Deskirpsi',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
			'm_product_type_id' => 'Tipe Produk',
			'm_customer_id' => 'Customer',
			'unit_price' => 'Harga',
		);
	}

}
