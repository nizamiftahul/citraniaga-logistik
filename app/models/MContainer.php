<?php

class MContainer extends ActiveRecord
{

	public function tableName()
	{
		return 'm_container';
	}

	public function rules()
	{
		return array(
			array('name, seal_status', 'required'),
			array('parent_id, m_container_type_id, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('spec_height, spec_length, spec_width', 'numerical'),
			array('name', 'length', 'max'=>45),
			array('seal_status', 'length', 'max'=>1),
			array('seal_date, created_date, updated_date', 'safe'),
		);
	}

	public function relations()
	{
		return array(
			'parent' => array(self::BELONGS_TO, 'MContainer', 'parent_id'),
			'mContainers' => array(self::HAS_MANY, 'MContainer', 'parent_id'),
			'mContainerType' => array(self::BELONGS_TO, 'MContainerType', 'm_container_type_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Nama',
			'seal_status' => 'Status Segel',
			'seal_date' => 'Tanggal Segel',
			'parent_id' => 'Parent',
			'm_container_type_id' => 'Tipe',
			'spec_height' => 'Tinggi',
			'spec_length' => 'Panjang',
			'spec_width' => 'Lebar',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

}
