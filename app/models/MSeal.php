<?php

class MSeal extends ActiveRecord
{

	public function tableName()
	{
		return 'm_seal';
	}

	public function rules()
	{
		return array(
			array('code', 'required'),
			array('created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('is_used, created_date, updated_date', 'safe'),
		);
	}

	public function relations()
	{
		return array(
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'code' => 'Kode',
			'is_used' => 'Is Used',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

}
