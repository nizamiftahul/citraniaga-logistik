<?php

class MOrde extends ActiveRecord
{

	public function tableName()
	{
		return 'm_orde';
	}

	public function rules()
	{
		return array(
			array('name, description', 'required'),
			array('created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
			array('created_date, updated_date', 'safe'),
		);
	}

	public function relations()
	{
		return array(
			'mRoutes' => array(self::HAS_MANY, 'MRoute', 'from_m_orde_id'),
			'mRoutes1' => array(self::HAS_MANY, 'MRoute', 'to_m_orde_id'),
			'mRoutePaths' => array(self::HAS_MANY, 'MRoutePath', 'm_orde_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Nama',
			'description' => 'Deskirpsi',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

}
