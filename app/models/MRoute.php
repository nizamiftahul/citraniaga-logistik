<?php

class MRoute extends ActiveRecord
{

	public function tableName()
	{
		return 'm_route';
	}

	public function rules()
	{
		return array(
			array('name, from_m_orde_id, to_m_orde_id', 'required'),
			array('from_m_orde_id, to_m_orde_id, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
			array('created_date, updated_date', 'safe'),
		);
	}

	public function relations()
	{
		return array(
			'fromMOrde' => array(self::BELONGS_TO, 'MOrde', 'from_m_orde_id'),
			'toMOrde' => array(self::BELONGS_TO, 'MOrde', 'to_m_orde_id'),
			'mRoutePaths' => array(self::HAS_MANY, 'MRoutePath', 'm_route_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'from_m_orde_id' => 'From M Orde',
			'to_m_orde_id' => 'To M Orde',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

}
