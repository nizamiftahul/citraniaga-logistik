<?php

class MQuotationDetails extends ActiveRecord
{

	public function tableName()
	{
		return 'm_quotation_details';
	}

	public function rules()
	{
		return array(
			array('m_quotation_id, m_cost_id, m_product_id', 'required'),
			array('m_quotation_id, m_cost_id, m_product_id, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('m_product_discounted_price', 'numerical'),
			array('created_date, updated_date', 'safe'),
		);
	}

	public function relations()
	{
		return array(
			'mQuotation' => array(self::BELONGS_TO, 'MQuotation', 'm_quotation_id'),
			'mCost' => array(self::BELONGS_TO, 'MCost', 'm_cost_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'm_quotation_id' => 'M Quotation',
			'm_cost_id' => 'M Cost',
			'm_product_id' => 'M Product',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
			'm_product_discounted_price' => 'M Product Discounted Price',
		);
	}

}
