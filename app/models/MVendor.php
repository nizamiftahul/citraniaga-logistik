<?php

class MVendor extends ActiveRecord
{

	public function tableName()
	{
		return 'm_vendor';
	}

	public function rules()
	{
		return array(
			array('name, phone', 'required'),
			array('created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>45),
			array('phone', 'length', 'max'=>12),
			array('description, created_date, updated_date', 'safe'),
		);
	}

	public function relations()
	{
		return array(
			'mCosts' => array(self::HAS_MANY, 'MCost', 'm_vendor_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Nama',
			'description' => 'Deskirpsi',
			'phone' => 'Telepon',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

}
