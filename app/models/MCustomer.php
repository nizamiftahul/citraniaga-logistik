<?php

class MCustomer extends ActiveRecord
{

	public function tableName()
	{
		return 'm_customer';
	}

	public function rules()
	{
		return array(
			array('name, address, npwp, phone', 'required'),
			array('created_by, updated_by, parent_id', 'numerical', 'integerOnly'=>true),
			array('name, username', 'length', 'max'=>100),
			array('npwp', 'length', 'max'=>45),
			array('phone', 'length', 'max'=>12),
			array('password', 'length', 'max'=>255),
			array('created_date, updated_date', 'safe'),
		);
	}

	public function relations()
	{
		return array(
			'mProducts' => array(self::HAS_MANY, 'MProduct', 'm_customer_id'),
			'parent' => array(self::BELONGS_TO, 'MCustomer', 'parent_id'),
			'mCustomers' => array(self::HAS_MANY, 'MCustomer', 'parent_id'),
			'mQuotations' => array(self::HAS_MANY, 'MQuotation', 'm_customer_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Nama',
			'address' => 'Alamat',
			'npwp' => 'NPWP',
			'phone' => 'Telepon',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
			'parent_id' => 'Parent',
			'username' => 'Username',
			'password' => 'Password',
		);
	}

}
