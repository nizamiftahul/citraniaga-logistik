<?php

Yii::import("app.modules.admin.forms.mVessel.*");

class MVesselController extends Controller {
    public function filters() {
        // Use access control filter
        return ['accessControl'];
    }

    public function accessRules() {
        // Only allow authenticated users
        return [['allow', 'users' => ['@']],['deny']];
    }
    
    public function actionIndex() {
        $this->renderForm('AdminMVesselIndex');
    }

    public function actionEdit($id = null) {
        if(is_null($id)){
            $model = new AdminMVesselForm;    
        } else {
            $model = $this->loadModel($id, "AdminMVesselForm");       
        }
        
        if (isset($_POST["AdminMVesselForm"])) {
            $model->attributes = $_POST["AdminMVesselForm"];
            if ($model->save()) {
                $this->flash('Data Berhasil Disimpan');
                $this->redirect(['index']);
            }
        }
        $this->renderForm("AdminMVesselForm", $model);
    }

    public function actionDelete($id) {
        if (strpos($id, ',') > 0) {
            ActiveRecord::batchDelete("AdminMVesselForm", explode(",", $id));
            $this->flash('Data Berhasil Dihapus');
        } else {
            $model = $this->loadModel($id, "AdminMVesselForm");
            if (!is_null($model)) {
                $this->flash('Data Berhasil Dihapus');
                $model->delete();
            }
        }


        $this->redirect(['index']);
    }
    
}
