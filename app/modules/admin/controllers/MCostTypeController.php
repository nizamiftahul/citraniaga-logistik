<?php

Yii::import("app.modules.admin.forms.mCostType.*");

class MCostTypeController extends Controller {
    public function filters() {
        // Use access control filter
        return ['accessControl'];
    }

    public function accessRules() {
        // Only allow authenticated users
        return [['allow', 'users' => ['@']],['deny']];
    }
    
    public function actionIndex() {
        $this->renderForm('AdminMCostTypeIndex');
    }

    public function actionEdit($id = null) {
        if(is_null($id)){
            $model = new AdminMCostTypeForm;    
        } else {
            $model = $this->loadModel($id, "AdminMCostTypeForm");
        }
        
        if (isset($_POST["AdminMCostTypeForm"])) {
            $model->attributes = $_POST["AdminMCostTypeForm"];
            
            if(is_null($id)){
                $model->created_by = Yii::app()->user->id;
                $model->created_date = date('Y-m-d H:i:s');
            } else {
                $model->updated_by = Yii::app()->user->id;
                $model->updated_date = date('Y-m-d H:i:s');
            }
            
            if ($model->save()) {
                $this->flash('Data Berhasil Disimpan');
                $this->redirect(['index']);
            }
        }
        $this->renderForm("AdminMCostTypeForm", $model);
    }

    public function actionDelete($id) {
        if (strpos($id, ',') > 0) {
            ActiveRecord::batchDelete("AdminMCostTypeForm", explode(",", $id));
            $this->flash('Data Berhasil Dihapus');
        } else {
            $model = $this->loadModel($id, "AdminMCostTypeForm");
            if (!is_null($model)) {
                $this->flash('Data Berhasil Dihapus');
                $model->delete();
            }
        }


        $this->redirect(['index']);
    }
    
}
