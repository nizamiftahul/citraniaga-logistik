<?php

Yii::import("app.modules.admin.forms.mSeal.*");

class MSealController extends Controller {
    public function filters() {
        // Use access control filter
        return ['accessControl'];
    }

    public function accessRules() {
        // Only allow authenticated users
        return [['allow', 'users' => ['@']],['deny']];
    }
    
    public function actionIndex() {
        $this->renderForm('AdminMSealIndex');
    }

    public function actionEdit($id = null) {
        if(is_null($id)){
            $model = new AdminMSealForm;    
        } else {
            $model = $this->loadModel($id, "AdminMSealForm");
            if ($model->is_used == false) $model->is_used = "Belum";
            else if ($model->is_used == true) $model->is_used = "Sudah";
        }
        
        if (isset($_POST["AdminMSealForm"])) {
            $model->attributes = $_POST["AdminMSealForm"];
            
            if(is_null($id)){
                $model->created_by = Yii::app()->user->id;
                $model->created_date = date('Y-m-d H:i:s');
            } else {
                $model->updated_by = Yii::app()->user->id;
                $model->updated_date = date('Y-m-d H:i:s');
            }
            
            if ($model->is_used == "Belum") $model->is_used = false;
            else if ($model->is_used == "Sudah") $model->is_used = true;
            
            if ($model->save()) {
                $this->flash('Data Berhasil Disimpan');
                $this->redirect(['index']);
            }
        }
        $this->renderForm("AdminMSealForm", $model);
    }

    public function actionDelete($id) {
        if (strpos($id, ',') > 0) {
            ActiveRecord::batchDelete("AdminMSealForm", explode(",", $id));
            $this->flash('Data Berhasil Dihapus');
        } else {
            $model = $this->loadModel($id, "AdminMSealForm");
            if (!is_null($model)) {
                $this->flash('Data Berhasil Dihapus');
                $model->delete();
            }
        }


        $this->redirect(['index']);
    }
    
}
