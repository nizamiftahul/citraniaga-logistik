<?php

Yii::import("app.modules.admin.forms.mCourierType.*");

class MCourierTypeController extends Controller {
    public function filters() {
        // Use access control filter
        return ['accessControl'];
    }

    public function accessRules() {
        // Only allow authenticated users
        return [['allow', 'users' => ['@']],['deny']];
    }
    
    public function actionIndex() {
        $this->renderForm('AdminMCourierTypeIndex');
    }

    public function actionEdit($id = null) {
        if(is_null($id)){
            $model = new AdminMCourierTypeForm;    
        } else {
            $model = $this->loadModel($id, "AdminMCourierTypeForm");       
        }
        
        if (isset($_POST["AdminMCourierTypeForm"])) {
            $model->attributes = $_POST["AdminMCourierTypeForm"];
            
            if(is_null($id)){
                $model->created_by = Yii::app()->user->id;
                $model->created_date = date('Y-m-d H:i:s');
            } else {
                $model->updated_by = Yii::app()->user->id;
                $model->updated_date = date('Y-m-d H:i:s');
            }
            
            if ($model->save()) {
                $this->flash('Data Berhasil Disimpan');
                $this->redirect(['index']);
            }
        }
        $this->renderForm("AdminMCourierTypeForm", $model);
    }

    public function actionDelete($id) {
        if (strpos($id, ',') > 0) {
            ActiveRecord::batchDelete("AdminMCourierTypeForm", explode(",", $id));
            $this->flash('Data Berhasil Dihapus');
        } else {
            $model = $this->loadModel($id, "AdminMCourierTypeForm");
            if (!is_null($model)) {
                $this->flash('Data Berhasil Dihapus');
                $model->delete();
            }
        }


        $this->redirect(['index']);
    }
    
}
