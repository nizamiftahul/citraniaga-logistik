<?php

Yii::import("app.modules.admin.forms.mCost.*");

class MCostController extends Controller {
    public function filters() {
        // Use access control filter
        return ['accessControl'];
    }

    public function accessRules() {
        // Only allow authenticated users
        return [['allow', 'users' => ['@']],['deny']];
    }
    
    public function actionIndex() {
        $this->renderForm('AdminMCostIndex');
    }

    public function actionEdit($id = null) {
        if(is_null($id)){
            $model = new AdminMCostForm;    
        } else {
            $model = $this->loadModel($id, "AdminMCostForm");
            if ($model->ppn_enabled == false) $model->ppn_enabled = "Tanpa PPN";
            else if ($model->ppn_enabled == true) $model->ppn_enabled = "PPN";
        }
        
        if (isset($_POST["AdminMCostForm"])) {
            $model->attributes = $_POST["AdminMCostForm"];
            
            if(is_null($id)){
                $model->created_by = Yii::app()->user->id;
                $model->created_date = date('Y-m-d H:i:s');
            } else {
                $model->updated_by = Yii::app()->user->id;
                $model->updated_date = date('Y-m-d H:i:s');
            }
            
            if ($model->ppn_enabled == "Tanpa PPN") $model->ppn_enabled = false;
            else if ($model->ppn_enabled == "PPN") $model->ppn_enabled = true;
            
            if ($model->save()) {
                $this->flash('Data Berhasil Disimpan');
                $this->redirect(['index']);
            }
        }
        $this->renderForm("AdminMCostForm", $model);
    }

    public function actionDelete($id) {
        if (strpos($id, ',') > 0) {
            ActiveRecord::batchDelete("AdminMCostForm", explode(",", $id));
            $this->flash('Data Berhasil Dihapus');
        } else {
            $model = $this->loadModel($id, "AdminMCostForm");
            if (!is_null($model)) {
                $this->flash('Data Berhasil Dihapus');
                $model->delete();
            }
        }


        $this->redirect(['index']);
    }
    
}
