<?php

Yii::import("app.modules.admin.forms.mCustomer.*");

class MCustomerController extends Controller {
    public function filters() {
        // Use access control filter
        return ['accessControl'];
    }

    public function accessRules() {
        // Only allow authenticated users
        return [['allow', 'users' => ['@']],['deny']];
    }
    
    public function actionIndex() {
        $this->renderForm('AdminMCustomerIndex');
    }

    public function actionEdit($id = null) {
        if(is_null($id)){
            $model = new AdminMCustomerForm;    
        } else {
            $model = $this->loadModel($id, "AdminMCustomerForm");       
        }
        
        if (isset($_POST["AdminMCustomerForm"])) {
            $model->attributes = $_POST["AdminMCustomerForm"];
            
            if(is_null($id)){
                $model->created_by = Yii::app()->user->id;
                $model->created_date = date('Y-m-d H:i:s');
            } else {
                $model->updated_by = Yii::app()->user->id;
                $model->updated_date = date('Y-m-d H:i:s');
            }
            
            if ($model->save()) {
                
                $dsChildrenInsert = json_decode($_POST["dsChildrenInsert"]);
                if (!is_null($dsChildrenInsert)) {
                    foreach ($dsChildrenInsert as $d) {
                        $child = MCustomer::model()->findByAttributes(["id" => $d->id]);
                        $child->parent_id = $model->id;
                        $child->save();
                    }
                }
                
                $dsChildrenDelete = json_decode($_POST["dsChildrenDelete"]);
                if (!is_null($dsChildrenDelete)) {
                    foreach ($dsChildrenDelete as $d) {
                        $child = MCustomer::model()->findByAttributes(["id" => $d->id]);
                        $child->parent_id = null;
                        $child->save();
                    }
                }
                
                $this->flash('Data Berhasil Disimpan');
                $this->redirect(['index']);
            }
        }
        $this->renderForm("AdminMCustomerForm", $model);
    }

    public function actionDelete($id) {
        if (strpos($id, ',') > 0) {
            ActiveRecord::batchDelete("AdminMCustomerForm", explode(",", $id));
            $this->flash('Data Berhasil Dihapus');
        } else {
            $model = $this->loadModel($id, "AdminMCustomerForm");
            if (!is_null($model)) {
                $this->flash('Data Berhasil Dihapus');
                $model->delete();
            }
        }


        $this->redirect(['index']);
    }
    
}
