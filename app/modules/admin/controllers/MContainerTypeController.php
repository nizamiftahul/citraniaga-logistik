<?php

Yii::import("app.modules.admin.forms.mContainerType.*");

class MContainerTypeController extends Controller {
    public function filters() {
        // Use access control filter
        return ['accessControl'];
    }

    public function accessRules() {
        // Only allow authenticated users
        return [['allow', 'users' => ['@']],['deny']];
    }
    
    public function actionIndex() {
        $this->renderForm('AdminMContainerTypeIndex');
    }

    public function actionEdit($id = null) {
        if(is_null($id)){
            $model = new AdminMContainerTypeForm;    
        } else {
            $model = $this->loadModel($id, "AdminMContainerTypeForm");       
        }
        
        if (isset($_POST["AdminMContainerTypeForm"])) {
            $model->attributes = $_POST["AdminMContainerTypeForm"];
            
            if(is_null($id)){
                $model->created_by = Yii::app()->user->id;
                $model->created_date = date('Y-m-d H:i:s');
            } else {
                $model->updated_by = Yii::app()->user->id;
                $model->updated_date = date('Y-m-d H:i:s');
            }
            
            if ($model->save()) {
                $this->flash('Data Berhasil Disimpan');
                $this->redirect(['index']);
            }
        }
        $this->renderForm("AdminMContainerTypeForm", $model);
    }

    public function actionDelete($id) {
        if (strpos($id, ',') > 0) {
            ActiveRecord::batchDelete("AdminMContainerTypeForm", explode(",", $id));
            $this->flash('Data Berhasil Dihapus');
        } else {
            $model = $this->loadModel($id, "AdminMContainerTypeForm");
            if (!is_null($model)) {
                $this->flash('Data Berhasil Dihapus');
                $model->delete();
            }
        }


        $this->redirect(['index']);
    }
    
}
