<?php

class AdminMCostForm extends MCost {

    public function getForm() {
        return array (
            'title' => 'Detail M Cost ',
            'layout' => array (
                'name' => 'full-width',
                'data' => array (
                    'col1' => array (
                        'type' => 'mainform',
                    ),
                ),
            ),
        );
    }

    public function getFields() {
        return array (
            array (
                'linkBar' => array (
                    array (
                        'label' => 'Kembali',
                        'icon' => 'chevron-left',
                        'options' => array (
                            'href' => 'url:/admin/mCost/index',
                        ),
                        'type' => 'LinkButton',
                    ),
                    array (
                        'label' => 'Simpan',
                        'buttonType' => 'success',
                        'icon' => 'check',
                        'options' => array (
                            'ng-click' => 'form.submit(this)',
                        ),
                        'type' => 'LinkButton',
                    ),
                    array (
                        'renderInEditor' => 'Yes',
                        'type' => 'Text',
                        'value' => '<div ng-if=\\"!isNewRecord\\" class=\\"separator\\"></div>',
                    ),
                    array (
                        'label' => 'Hapus',
                        'buttonType' => 'danger',
                        'icon' => 'trash',
                        'options' => array (
                            'ng-if' => '!isNewRecord',
                            'href' => 'url:/admin/mCost/delete?id={model.id}',
                            'confirm' => 'Apakah Anda Yakin ?',
                        ),
                        'type' => 'LinkButton',
                    ),
                ),
                'title' => '{{ isNewRecord ? \'Tambah Biaya\' : \'Update Biaya\'}}',
                'type' => 'ActionBar',
            ),
            array (
                'name' => 'id',
                'type' => 'HiddenField',
            ),
            array (
                'column1' => array (
                    array (
                        'label' => 'Nama',
                        'name' => 'name',
                        'type' => 'TextField',
                    ),
                    array (
                        'label' => 'Vendor',
                        'name' => 'm_vendor_id',
                        'modelClass' => 'app.models.MVendor',
                        'idField' => 'id',
                        'labelField' => 'name',
                        'type' => 'RelationField',
                    ),
                    array (
                        'label' => 'Cost Pol',
                        'name' => 'pol_cost',
                        'minValue' => '0',
                        'type' => 'NumberField',
                    ),
                    array (
                        'label' => 'Cost Pod',
                        'name' => 'pod_cost',
                        'minValue' => '0',
                        'type' => 'NumberField',
                    ),
                    array (
                        'label' => 'Worker Pod',
                        'name' => 'worker_pod',
                        'minValue' => '0',
                        'type' => 'NumberField',
                    ),
                    array (
                        'label' => 'Trucking Pod',
                        'name' => 'trucking_pod',
                        'minValue' => '0',
                        'type' => 'NumberField',
                    ),
                    array (
                        'label' => 'Tax Cost',
                        'name' => 'tax_cost',
                        'minValue' => '0',
                        'type' => 'NumberField',
                    ),
                    array (
                        'label' => 'PPN',
                        'name' => 'ppn_enabled',
                        'onLabel' => 'PPN',
                        'offLabel' => 'Tanpa PPN',
                        'type' => 'ToggleSwitch',
                    ),
                    array (
                        'type' => 'Text',
                        'value' => '<column-placeholder></column-placeholder>',
                    ),
                ),
                'column2' => array (
                    array (
                        'label' => 'Tipe',
                        'name' => 'm_cost_type_id',
                        'modelClass' => 'app.models.MCostType',
                        'idField' => 'id',
                        'labelField' => 'name',
                        'type' => 'RelationField',
                    ),
                    array (
                        'label' => 'Tipe Kontainer',
                        'name' => 'm_container_type_id',
                        'modelClass' => 'app.models.MContainerType',
                        'idField' => 'id',
                        'labelField' => 'name',
                        'type' => 'RelationField',
                    ),
                    array (
                        'label' => 'Trucking Pol',
                        'name' => 'trucking_pol',
                        'minValue' => '0',
                        'type' => 'NumberField',
                    ),
                    array (
                        'label' => 'Pelindo Pod',
                        'name' => 'pelindo_pod',
                        'minValue' => '0',
                        'type' => 'NumberField',
                    ),
                    array (
                        'label' => 'Dooring Pod',
                        'name' => 'dooring_pod',
                        'minValue' => '0',
                        'type' => 'NumberField',
                    ),
                    array (
                        'label' => 'Other Cost',
                        'name' => 'other_cost',
                        'minValue' => '0',
                        'type' => 'NumberField',
                    ),
                    array (
                        'label' => 'Asuransi',
                        'name' => 'insurance',
                        'minValue' => '0',
                        'type' => 'NumberField',
                    ),
                    array (
                        'label' => 'Keterangan',
                        'name' => 'description',
                        'fieldOptions' => array (
                            'auto-grow' => 'true',
                        ),
                        'type' => 'TextArea',
                    ),
                    array (
                        'type' => 'Text',
                        'value' => '<column-placeholder></column-placeholder>',
                    ),
                ),
                'w1' => '50%',
                'w2' => '50%',
                'type' => 'ColumnField',
            ),
        );
    }

}