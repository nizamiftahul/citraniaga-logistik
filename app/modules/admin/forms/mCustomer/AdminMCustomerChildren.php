<?php

class AdminMCustomerChildren extends Form {

    public function getForm() {
        return array (
            'title' => 'M Customer Children',
            'layout' => array (
                'name' => 'full-width',
                'data' => array (
                    'col1' => array (
                        'type' => 'mainform',
                    ),
                ),
            ),
        );
    }

    public function getFields() {
        return array (
            array (
                'column1' => array (
                    array (
                        'type' => 'Text',
                        'value' => '<column-placeholder></column-placeholder>',
                    ),
                    array (
                        'label' => 'Customer',
                        'name' => 'id',
                        'relationCriteria' => array (
                            'select' => '',
                            'distinct' => 'false',
                            'alias' => 't',
                            'condition' => 'parent_id is null {and id <> :parent_id} {and [search]}',
                            'order' => '',
                            'group' => '',
                            'having' => '',
                            'join' => '',
                        ),
                        'params' => array (
                            ':parent_id' => 'js: params.id',
                        ),
                        'modelClass' => 'app.models.MCustomer',
                        'idField' => 'id',
                        'labelField' => 'name',
                        'type' => 'RelationField',
                    ),
                ),
                'w1' => '50%',
                'w2' => '50%',
                'type' => 'ColumnField',
            ),
        );
    }

}