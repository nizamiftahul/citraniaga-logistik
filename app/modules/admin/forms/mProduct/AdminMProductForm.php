<?php

class AdminMProductForm extends MProduct {

    public function getForm() {
        return array (
            'title' => 'Detail M Product ',
            'layout' => array (
                'name' => 'full-width',
                'data' => array (
                    'col1' => array (
                        'type' => 'mainform',
                    ),
                ),
            ),
        );
    }

    public function getFields() {
        return array (
            array (
                'linkBar' => array (
                    array (
                        'label' => 'Kembali',
                        'icon' => 'chevron-left',
                        'options' => array (
                            'href' => 'url:/admin/mProduct/index',
                        ),
                        'type' => 'LinkButton',
                    ),
                    array (
                        'label' => 'Simpan',
                        'buttonType' => 'success',
                        'icon' => 'check',
                        'options' => array (
                            'ng-click' => 'form.submit(this)',
                        ),
                        'type' => 'LinkButton',
                    ),
                    array (
                        'renderInEditor' => 'Yes',
                        'type' => 'Text',
                        'value' => '<div ng-if=\\"!isNewRecord\\" class=\\"separator\\"></div>',
                    ),
                    array (
                        'label' => 'Hapus',
                        'buttonType' => 'danger',
                        'icon' => 'trash',
                        'options' => array (
                            'ng-if' => '!isNewRecord',
                            'href' => 'url:/admin/mProduct/delete?id={model.id}',
                            'confirm' => 'Apakah Anda Yakin ?',
                        ),
                        'type' => 'LinkButton',
                    ),
                ),
                'title' => '{{ isNewRecord ? \'Tambah Produk\' : \'Update Produk\'}}',
                'type' => 'ActionBar',
            ),
            array (
                'name' => 'id',
                'type' => 'HiddenField',
            ),
            array (
                'column1' => array (
                    array (
                        'label' => 'Name',
                        'name' => 'name',
                        'type' => 'TextField',
                    ),
                    array (
                        'label' => 'Customer',
                        'name' => 'm_customer_id',
                        'modelClass' => 'app.models.MCustomer',
                        'idField' => 'id',
                        'labelField' => 'name',
                        'type' => 'RelationField',
                    ),
                    array (
                        'label' => 'Deskripsi',
                        'name' => 'description',
                        'fieldOptions' => array (
                            'auto-grow' => 'true',
                        ),
                        'type' => 'TextArea',
                    ),
                    array (
                        'type' => 'Text',
                        'value' => '<column-placeholder></column-placeholder>',
                    ),
                ),
                'column2' => array (
                    array (
                        'label' => 'Tipe',
                        'name' => 'm_product_type_id',
                        'modelClass' => 'app.models.MProductType',
                        'idField' => 'id',
                        'labelField' => 'name',
                        'type' => 'RelationField',
                    ),
                    array (
                        'label' => 'Harga',
                        'name' => 'unit_price',
                        'prefix' => 'Rp',
                        'type' => 'NumberField',
                        'minValue' => '0',
                    ),
                    array (
                        'type' => 'Text',
                        'value' => '<column-placeholder></column-placeholder>',
                    ),
                ),
                'w1' => '50%',
                'w2' => '50%',
                'type' => 'ColumnField',
            ),
        );
    }

}