<?php

class AdminMContainerForm extends MContainer {

    public function getForm() {
        return array (
            'title' => 'Detail M Container ',
            'layout' => array (
                'name' => 'full-width',
                'data' => array (
                    'col1' => array (
                        'type' => 'mainform',
                    ),
                ),
            ),
        );
    }

    public function getFields() {
        return array (
            array (
                'linkBar' => array (
                    array (
                        'label' => 'Kembali',
                        'icon' => 'chevron-left',
                        'options' => array (
                            'href' => 'url:/admin/mContainer/index',
                        ),
                        'type' => 'LinkButton',
                    ),
                    array (
                        'label' => 'Simpan',
                        'buttonType' => 'success',
                        'icon' => 'check',
                        'options' => array (
                            'ng-click' => 'form.submit(this)',
                        ),
                        'type' => 'LinkButton',
                    ),
                    array (
                        'renderInEditor' => 'Yes',
                        'type' => 'Text',
                        'value' => '<div ng-if=\\"!isNewRecord\\" class=\\"separator\\"></div>',
                    ),
                    array (
                        'label' => 'Hapus',
                        'buttonType' => 'danger',
                        'icon' => 'trash',
                        'options' => array (
                            'ng-if' => '!isNewRecord',
                            'href' => 'url:/admin/mContainer/delete?id={model.id}',
                            'confirm' => 'Apakah Anda Yakin ?',
                        ),
                        'type' => 'LinkButton',
                    ),
                ),
                'title' => '{{ isNewRecord ? \'Tambah Kontainer\' : \'Update Kontainer\'}}',
                'type' => 'ActionBar',
            ),
            array (
                'name' => 'id',
                'type' => 'HiddenField',
            ),
            array (
                'column1' => array (
                    array (
                        'label' => 'Nama',
                        'name' => 'name',
                        'type' => 'TextField',
                    ),
                    array (
                        'label' => 'Tinggi',
                        'name' => 'spec_height',
                        'minValue' => '0',
                        'type' => 'NumberField',
                    ),
                    array (
                        'label' => 'Lebar',
                        'name' => 'spec_width',
                        'minValue' => '0',
                        'type' => 'NumberField',
                    ),
                    array (
                        'label' => 'Tanggal Segel',
                        'name' => 'seal_date',
                        'options' => array (
                            'ng-if' => 'model.seal_status === \'S\'',
                        ),
                        'type' => 'DateTimePicker',
                    ),
                    array (
                        'type' => 'Text',
                        'value' => '<column-placeholder></column-placeholder>',
                    ),
                ),
                'column2' => array (
                    array (
                        'label' => 'Tipe',
                        'name' => 'm_container_type_id',
                        'modelClass' => 'app.models.MContainerType',
                        'idField' => 'id',
                        'labelField' => 'name',
                        'type' => 'RelationField',
                    ),
                    array (
                        'label' => 'Panjang',
                        'name' => 'spec_length',
                        'minValue' => '0',
                        'type' => 'NumberField',
                    ),
                    array (
                        'label' => 'Status Segel',
                        'name' => 'seal_status',
                        'defaultType' => 'first',
                        'list' => array (
                            'S' => 'Sealed',
                            'N' => 'Not Sealed',
                        ),
                        'type' => 'DropDownList',
                    ),
                    array (
                        'type' => 'Text',
                        'value' => '<column-placeholder></column-placeholder>',
                    ),
                ),
                'w1' => '50%',
                'w2' => '50%',
                'type' => 'ColumnField',
            ),
        );
    }

}